

export class MyWebComp extends HTMLElement {


    constructor() {
        super();
    }

    connectedCallback() {
        let html = document.importNode(myWebCompTemplate.content, true);
        this.attachShadow({mode: 'open'});
        this.shadowRoot.appendChild(html);
        this.updateLabel();
        this.counter.addEventListener('countChanged', this.updateLabel.bind(this));
        document.addEventListener('countChanged', this.updateLabel.bind(this));
    }

    disconnectedCallback() {
        document.removeEventListener(new CustomEvent('increment'));
        document.removeEventListener(new CustomEvent('countChanged'));
    }

    updateLabel() {
        this.shadowRoot.querySelector('#helloLabel').textContent = 'Hello ' +
            this.getAttribute('greet-name') + ' ' + this.getAttribute('count') + ' ' + this.counter.count;
    }

    static get observedAttributes() {
        return ['count'];
    }

    attributeChangedCallback(name, oldValue, newValue) {
        if (name === 'count') {
            if (this.shadowRoot) {
                this.updateLabel();
            }
        }
    }

    showMessage(event) {
        this.setAttribute('count', parseInt(this.getAttribute('count')) + 1);
        this.counter.dispatchEvent(new CustomEvent('increment'));
        this.dispatchEvent(new CustomEvent('increment', { bubbles: true }));
        document.dispatchEvent(new CustomEvent('increment'));
    }
}