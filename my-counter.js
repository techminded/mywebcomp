

export class MyCounter extends EventTarget {

    constructor() {
        super();
        this.count = 0;
        this.addEventListener('increment', this.increment.bind(this));
        document.addEventListener('increment', this.increment.bind(this));
    }

    increment() {
        this.count++;
        this.dispatchEvent(new CustomEvent('countChanged', {
            detail: { count: this.count }
        }));
        document.dispatchEvent(new CustomEvent('countChanged', {
            detail: { count: this.count }
        }));
    }

}