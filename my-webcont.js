

export class MyWebCont extends HTMLElement {


    constructor() {
        super();
    }

    connectedCallback() {
        this.addEventListener('increment', this.updateCount.bind(this));
    }

    updateCount(event) {
        console.log('MyWebCont', event);
        this.setAttribute('count', parseInt(this.getAttribute('count')) + 1);
    }

    static get observedAttributes() {
        return ['count'];
    }

    attributeChangedCallback(name, oldValue, newValue) {
        if (name === 'count') {
            this.querySelectorAll('my-webcomp').forEach((el) => {
               el.setAttribute('count', newValue);
            });
        }
    }

}